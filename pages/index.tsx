/* eslint-disable no-nested-ternary */
import React from "react"

import dynamic from "next/dynamic"
import useSWR from "swr"

import { makeStyles, Theme } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"

const Loading = () => <div>loading...</div>
const DynamicUsersTable = dynamic(() => import("components/UsersTable"), {
  loading: () => <Loading />,
})

const fetcher = (input: RequestInfo, init?: RequestInit) =>
  fetch(input, init).then((res) => res.json())

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    paddingTop: theme.spacing(4),
  },
}))

const Home = () => {
  const classes = useStyles()
  const { data, error } = useSWR(
    "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json",
    fetcher,
  )

  return (
    <Container className={classes.container}>
      {error ? (
        <div>failed to load</div>
      ) : !data ? (
        <Loading />
      ) : (
        <DynamicUsersTable data={data} />
      )}
    </Container>
  )
}

export default Home

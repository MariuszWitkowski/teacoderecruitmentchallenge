import React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import TableCell from "@material-ui/core/TableCell"
import {
  AutoSizer,
  Column,
  Table,
  TableCellProps,
  TableHeaderProps,
} from "react-virtualized"

const useStyles = makeStyles((theme: Theme) => ({
  flexContainer: {
    display: "flex",
    alignItems: "center",
    boxSizing: "border-box",
  },
  tableRow: {
    cursor: "pointer",
  },
  tableRowHover: {
    "&:hover": {
      backgroundColor: theme.palette.grey[200],
    },
  },
  tableCell: {
    flex: 1,
  },
  noClick: {
    cursor: "initial",
  },
}))

interface ColumnData {
  dataKey: string
  label: string
  width: number
}

interface Row {
  index: number
}

interface MuiVirtualizedTableProps {
  columns: ColumnData[]
  headerHeight?: number
  rowCount: number
  rowGetter: (row: Row) => any // FIXME:
  rowHeight?: number
  headerRenderer: (headerProps: TableHeaderProps) => JSX.Element | undefined
  cellRenderer: (cellProps: TableCellProps) => JSX.Element | undefined
}

const defaultHeaderRenderer = ({
  label,
}: TableHeaderProps): JSX.Element | undefined => (
  <TableCell variant="head" component="div">
    <span>{label}</span>
  </TableCell>
)

const condHeaderRenderer = (
  renderer?: MuiVirtualizedTableProps["headerRenderer"],
) => (headerProps: TableHeaderProps): JSX.Element =>
  renderer
    ? renderer(headerProps) || defaultHeaderRenderer(headerProps)
    : defaultHeaderRenderer(headerProps)

const defaultCellRenderer = ({ cellData }: TableCellProps) => {
  return <TableCell variant="body">{cellData}</TableCell>
}

const condCellRenderer = (
  renderer?: MuiVirtualizedTableProps["cellRenderer"],
) => (cellProps: TableCellProps): JSX.Element =>
  renderer
    ? renderer(cellProps) || defaultCellRenderer(cellProps)
    : defaultCellRenderer(cellProps)

const VirtualizedTable = ({
  columns,
  rowHeight,
  headerHeight,
  headerRenderer,
  cellRenderer,
  ...tableProps
}: MuiVirtualizedTableProps): JSX.Element => {
  const classes = useStyles()

  const getRowClassName = () => {
    return [classes.tableRow, classes.flexContainer].join(" ")
  }

  return (
    <AutoSizer>
      {({ height, width }) => (
        <Table
          height={height}
          width={width}
          rowHeight={rowHeight!}
          gridStyle={{
            direction: "inherit",
          }}
          headerHeight={headerHeight!}
          {...tableProps}
          rowClassName={getRowClassName}
        >
          {columns.map(({ dataKey, ...other }) => {
            return (
              <Column
                key={dataKey}
                headerRenderer={condHeaderRenderer(headerRenderer)}
                className={classes.flexContainer}
                cellRenderer={condCellRenderer(cellRenderer)}
                dataKey={dataKey}
                {...other}
              />
            )
          })}
        </Table>
      )}
    </AutoSizer>
  )
}

VirtualizedTable.defaultProps = {
  headerHeight: 48,
  rowHeight: 48,
}

export default VirtualizedTable

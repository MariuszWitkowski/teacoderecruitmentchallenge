/* eslint-disable react/prop-types */
/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from "react"

import { makeStyles } from "@material-ui/core/styles"
import TableCell from "@material-ui/core/TableCell"
import Toolbar from "@material-ui/core/Toolbar"
import Paper from "@material-ui/core/Paper"
import Checkbox from "@material-ui/core/Checkbox"
import InputBase from "@material-ui/core/InputBase"
import IconButton from "@material-ui/core/IconButton"
import SearchIcon from "@material-ui/icons/Search"

import { TableHeaderProps, TableCellProps } from "react-virtualized"

import VirtualizedTable from "components/VirtualizedTable"

type RowData = {
  id: number
  first_name: string
  last_name: string
  email: string
  gender: string
  avatar: string
}

const useDebounce = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    return () => {
      clearTimeout(handler)
    }
  }, [value])

  return debouncedValue
}

const includeQuery = (text: string | undefined, query: string) =>
  text && text.toLowerCase().includes(query.toLowerCase())
const filterData = (data, query) =>
  query
    ? data.filter(
        (record: RowData) =>
          includeQuery(record.first_name, query) ||
          includeQuery(record.last_name, query),
      )
    : data

const useData = (data: RowData[]) => {
  const [selected, setSelected] = useState<number[]>([])
  const [query, setQuery] = useState<string>("")
  const [filteredData, setFilteredData] = useState<RowData[]>(
    filterData(data, query),
  )
  const debouncedSearch = useDebounce(query, 500)
  useEffect(() => {
    setFilteredData(filterData(data, debouncedSearch))
  }, [debouncedSearch])
  const sortedData = filteredData.sort((a: RowData, b: RowData): number =>
    a.last_name > b.last_name ? 1 : a.last_name < b.last_name ? -1 : 0,
  )
  const inputChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setQuery(event.target.value)

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = sortedData.map((n) => n.id)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }
  const handleClick = (event: React.MouseEvent<unknown>, name: number) => {
    const selectedIndex = selected.indexOf(name)
    let newSelected: number[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      )
    }

    // eslint-disable-next-line no-console
    console.log("Selected IDs", newSelected)

    setSelected(newSelected)
  }

  const isSelected = (name: number) => selected.indexOf(name) !== -1

  const rowCount = sortedData ? sortedData.length : 0
  const numSelected = selected.length
  const isIndeterminateSelected = numSelected > 0 && numSelected < rowCount
  const isAllSelected = rowCount > 0 && numSelected === rowCount

  const rowGetter = ({ index }) => sortedData[index]

  return {
    rowGetter,
    isAllSelected,
    isIndeterminateSelected,
    rowCount,
    query,
    search: inputChange,
    selectAll: handleSelectAllClick,
    select: handleClick,
    isSelected,
  }
}

const useStyles = makeStyles({
  flexContainer: {
    display: "flex",
    alignItems: "center",
    boxSizing: "border-box",
  },
  tableCell: {
    flex: 1,
  },
})

const UsersTable = ({ data }) => {
  const classes = useStyles()
  const {
    rowGetter,
    isAllSelected,
    isIndeterminateSelected,
    rowCount,
    query,
    search,
    selectAll,
    select,
    isSelected,
  } = useData(data)

  const rowHeight = 82

  const headerRenderer = ({
    dataKey,
  }: TableHeaderProps): JSX.Element | undefined =>
    dataKey === "checkbox" ? (
      <TableCell padding="checkbox" variant="head" component="div">
        <Checkbox
          indeterminate={isIndeterminateSelected}
          checked={isAllSelected}
          onChange={selectAll}
          inputProps={{ "aria-label": "select all desserts" }}
        />
      </TableCell>
    ) : undefined

  const cellRenderer = ({
    cellData,
    dataKey,
    rowData,
    rowIndex,
  }: TableCellProps): JSX.Element | undefined => {
    const isItemSelected = isSelected(rowData.id)
    const labelId = `enhanced-table-checkbox-${rowIndex}`
    const fullname = `${rowData.first_name} ${rowData.last_name}`

    let cellValue
    switch (dataKey) {
      case "avatar":
        cellValue = <img src={cellData} alt={fullname} />
        break
      case "fullname":
        cellValue = <span id={labelId}>{fullname}</span>
        break
      case "checkbox":
        cellValue = (
          <Checkbox
            checked={isItemSelected}
            inputProps={{ "aria-labelledby": labelId }}
          />
        )
        break
      default:
        break
    }
    return cellValue ? (
      <TableCell
        component="div"
        onClick={(event) => select(event, rowData.id)}
        className={[classes.tableCell, classes.flexContainer].join(" ")}
        variant="body"
        style={{ height: rowHeight }}
      >
        {cellValue}
      </TableCell>
    ) : undefined
  }

  const columns = [
    {
      width: 50,
      label: "Avatar",
      dataKey: "avatar",
    },
    {
      width: 300,
      label: "Fullname",
      dataKey: "fullname",
    },
    {
      width: 120,
      label: "",
      dataKey: "checkbox",
    },
  ]

  return (
    <>
      <Toolbar>
        <InputBase
          placeholder="Search"
          inputProps={{ "aria-label": "search" }}
          value={query}
          onChange={search}
        />
        <IconButton type="submit" aria-label="search">
          <SearchIcon />
        </IconButton>
      </Toolbar>
      <Paper style={{ height: 600, width: "100%" }}>
        <VirtualizedTable
          rowHeight={rowHeight}
          rowCount={rowCount}
          rowGetter={rowGetter}
          columns={columns}
          headerRenderer={headerRenderer}
          cellRenderer={cellRenderer}
        />
      </Paper>
    </>
  )
}

export default UsersTable
